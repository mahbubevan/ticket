const express = require('express');
const router = express.Router();

router.get('/',function(req,res){
  req.session.username = null;
  res.redirect('/');
});
module.exports = router;
