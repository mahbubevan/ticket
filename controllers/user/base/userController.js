const express = require('express');
const router = express.Router();

router.get('*',function(req,res,next){
  if(req.session.username==null){
    res.redirect('/userlogin');
  }else{
    next();
  }
});

router.get('/',function(req,res){
  var user = {
    name:req.session.username,
  };
  res.render('user/base/index',user);
});

module.exports = router;
