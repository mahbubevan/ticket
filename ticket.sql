-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2018 at 05:39 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`, `role`) VALUES
(1, 'evan', 'evan', 'ceo'),
(2, 'omiq', 'omiq', 'ceo');

-- --------------------------------------------------------

--
-- Table structure for table `air`
--

CREATE TABLE `air` (
  `id` int(11) NOT NULL,
  `flightname` varchar(200) NOT NULL,
  `departure` varchar(200) NOT NULL,
  `arrival` varchar(200) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `type` varchar(200) NOT NULL,
  `cost` int(20) NOT NULL,
  `costtype` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `air`
--

INSERT INTO `air` (`id`, `flightname`, `departure`, `arrival`, `duration`, `date`, `type`, `cost`, `costtype`) VALUES
(1, 'EK', 'PBH', 'CGP', '27h 24min', '2018-10-28', 'business', 18000, 'norefund'),
(2, 'BS', 'CGP', 'GOI', '6hr 2min', '2018-10-31', 'economic', 20000, 'refund'),
(3, 'QR', 'CGP', 'SIN', '', '0000-00-00', '', 0, ''),
(4, 'QR', 'DAC', 'SIN', '4hr 20min', '2018-11-02', 'business', 0, ''),
(5, 'QR', 'CGP', 'PBH', '4hr 20min', '2018-11-02', 'economic', 20000, 'refund'),
(6, 'Emirates', '4', '3', '2hr', '2018-11-01', 'business', 15000, 'norefund'),
(7, 'Biman Bangladesh Airlines', '2', '2', '3hr', '2018-11-05', 'economic', 50000, 'refund'),
(8, 'Biman Bangladesh Airlines', 'Osmani International Airport', 'Osmani International Airport', '4hr', '2018-11-05', 'business', 5000, 'norefund');

-- --------------------------------------------------------

--
-- Table structure for table `airportlist`
--

CREATE TABLE `airportlist` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `shortname` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airportlist`
--

INSERT INTO `airportlist` (`id`, `name`, `shortname`, `country`) VALUES
(2, 'Osmani International Airport', 'ZYL', 'Bangladesh'),
(3, 'Shah Amanat International Airport something', 'CGP', 'Bangladesh'),
(4, 'Hazrat Shahjalal International Airport', 'DAC', 'Bangladesh'),
(5, 'Paro Airport', 'PBH', 'BHUTAN'),
(6, 'Goa International Airport', 'GOI', 'India'),
(8, 'Singapore Changi Airport', 'SIN', 'Singapore');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `ticketid` int(20) NOT NULL,
  `userid` int(20) NOT NULL,
  `invoiceid` int(20) NOT NULL,
  `paymenttype` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `adultcount` int(20) NOT NULL,
  `total_amount` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `ticketid`, `userid`, `invoiceid`, `paymenttype`, `status`, `adultcount`, `total_amount`) VALUES
(1, 8, 1, 1, 'master-card', 'paid', 2, '10000');

-- --------------------------------------------------------

--
-- Table structure for table `flightname`
--

CREATE TABLE `flightname` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `cost` int(20) NOT NULL,
  `costtype` varchar(200) NOT NULL,
  `shortname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flightname`
--

INSERT INTO `flightname` (`id`, `name`, `cost`, `costtype`, `shortname`) VALUES
(1, 'Biman Bangladesh Airlines', 200, 'economic', 'BG'),
(2, 'US-Bangla Airlines', 7000, 'economic', 'BS'),
(3, 'Emirates', 12000, 'business', 'EK'),
(4, 'Novo Air', 2500, 'economic', 'VQ'),
(5, 'Qatar Airways', 5000, 'business', 'QR'),
(6, 'hb', -2, 'economic', 'jh');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `secretkey` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `secretkey`, `email`) VALUES
(1, 'evan', 'evan', '0', 'evan@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `air`
--
ALTER TABLE `air`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airportlist`
--
ALTER TABLE `airportlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flightname`
--
ALTER TABLE `flightname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `secretkey` (`secretkey`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `air`
--
ALTER TABLE `air`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `airportlist`
--
ALTER TABLE `airportlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `flightname`
--
ALTER TABLE `flightname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
